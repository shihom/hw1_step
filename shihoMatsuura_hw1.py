'''
STEP16 HW1
Name: Shiho Matsuura
Program: return longest word(s) found in jumbled input (of 16 letters)
'''

#reads in dictionary returns list of words
def open_file(filename):
    with open(filename, "r") as f:
        return f.read().split("\n")
            
#returns words w/ under X letters
def shorter_dict(word_list, max, min): 
    return [word for word in word_list if len(word) <= max and len(word) >= min]
            
#sorts words by length
def sorted_words(word_list): 
    return sorted(word_list, key = len, reverse = True)

#returns longest words found in user input
def find_word(input, sorted_list):
	words_found = [] #list of words found
	length_of_found_word = 0 #keep shorter words from being returned
	for word in sorted_list:
		temp_str = word
		if len(word) < length_of_found_word:
			return words_found
		for char in input: 
			if char in word: #loop thru chars in word
				temp_str = temp_str.replace(char, "", 1) #delete if also found in input
		if temp_str == "": #when all chars in str are gone, append to return list
			words_found.append(word)
			length_of_found_word = len(word)
            
def main():
    print '''\n************************************************************************************
    This program returns the longest word(s) found in a jumble set of 16 letters. 
************************************************************************************\n'''
    
    filename = "dictionary.txt"
    word_list = open_file(filename)
    short_list = shorter_dict(word_list, 7, 2)
    sorted_list = sorted_words(short_list)
    input = raw_input("Enter 16 random letters: ")
    
    while len(input) != 16:
        input = raw_input("Number of letters must be 16: ")
    print find_word(input, sorted_list)

    
if __name__ == "__main__":
    main()